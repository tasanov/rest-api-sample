package com.pps.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pps.object.ResponseStatus;
import com.pps.object.request.AcceptDisclaimerRequest;
import com.pps.object.response.AcceptDisclaimerResponse;

@RestController
@RequestMapping("/login")
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@RequestMapping(value = "/acceptDisclaimer", method = RequestMethod.POST)
	@PreAuthorize("@acceptDisclaimerAuthorizationService.checkAuthorization(principal, #in)")
	public AcceptDisclaimerResponse acceptDisclaimer(@RequestBody AcceptDisclaimerRequest in) {
		logger.info("from [" + in.getAppId() + "]");
		
		AcceptDisclaimerResponse response = new AcceptDisclaimerResponse();
		
		try {
			// TODO PAS accept disclaimer
			
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			response.setStatus(ResponseStatus.FAILURE);
			response.setErrorMessage(e.getMessage());
		}
		
		return response;
	}
}
