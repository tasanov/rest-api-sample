package com.pps.controller;

import java.util.Arrays;

import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pps.object.ResponseStatus;
import com.pps.object.request.AuthenticateRequest;
import com.pps.object.request.PASRequest;
import com.pps.object.response.AuthenticateResponse;
import com.pps.token.PASTokenService;

@RestController
public class PublicController {
	
	private static final Logger logger = LoggerFactory.getLogger(PublicController.class);
	
	@Autowired
	private PASTokenService pasTokenService;
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public AuthenticateResponse authenticate(@RequestBody AuthenticateRequest in) {
		logger.info("user [" + in.getUserId() + "] from [" + in.getAppId() + "]");
		
		AuthenticateResponse response = new AuthenticateResponse();
		
		try {
			// TODO authenticate to PAS
			
			Arrays.fill(in.getPassword(), '\u0000');
			
			String token = this.pasTokenService.generateAuthToken(in);
			
			response.setStatus(ResponseStatus.SUCCESS);
			response.setIdToken(token);
		} catch (JoseException e) {
			logger.error(e.getMessage(), e);
			
			response.setStatus(ResponseStatus.FAILURE);
			response.setErrorMessage(e.getMessage());
			response.setIdToken(null);
		}

		return response;
	}
	
	@RequestMapping(value = "/requestToken", method = RequestMethod.POST)
	public AuthenticateResponse requestTempToken(@RequestBody PASRequest in) {
		logger.info("from [" + in.getAppId() + "]");
		
		AuthenticateResponse response = new AuthenticateResponse();
		
		try {
			String token = this.pasTokenService.generateTempToken(in.getAppId());
			
			response.setStatus(ResponseStatus.SUCCESS);
			response.setIdToken(token);
		} catch (JoseException e) {
			logger.error(e.getMessage(), e);
			
			response.setStatus(ResponseStatus.FAILURE);
			response.setErrorMessage(e.getMessage());
			response.setIdToken(null);
		}

		return response;
	}
}
