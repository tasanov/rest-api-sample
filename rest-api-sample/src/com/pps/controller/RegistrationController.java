package com.pps.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pps.object.ResponseStatus;
import com.pps.object.request.CheckUsernameRequest;
import com.pps.object.response.CheckUsernameResponse;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);
	
	@RequestMapping(value = "/checkUsername", method = RequestMethod.POST)
	@PreAuthorize("@checkUsernameAuthorizationService.checkAuthorization(principal, #in)")
	public CheckUsernameResponse checkUsername(@RequestBody CheckUsernameRequest in) {
		logger.info("from [" + in.getAppId() + "] check [" + in.getUsername() + "]");
		
		CheckUsernameResponse response = new CheckUsernameResponse();
		
		try {
			// TODO check in PAS
			response.setAvailable(true);
			
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			response.setStatus(ResponseStatus.FAILURE);
			response.setErrorMessage(e.getMessage());
		}
		
		return response;
	}
}
