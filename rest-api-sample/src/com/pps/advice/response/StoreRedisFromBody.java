package com.pps.advice.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.pps.redis.RedisService;
import com.pps.token.PASTokenService;

@ControllerAdvice
public class StoreRedisFromBody implements ResponseBodyAdvice<StoreRedisFromBodyIdentifier> {

	private static final Logger logger = LoggerFactory.getLogger(StoreRedisFromBody.class);
	
	@Autowired
	private PASTokenService pasTokenService;
	
	@Autowired
	private RedisService redisService;
	
	private static final Class<?> myClazz = StoreRedisFromBodyIdentifier.class;
	
	@Override
	public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> converterType) {
		Class<?> clazz = methodParameter.getMethod().getReturnType();
		
		return myClazz.isAssignableFrom(clazz);
	}

	@Override
	public StoreRedisFromBodyIdentifier beforeBodyWrite(StoreRedisFromBodyIdentifier body, MethodParameter methodParameter, MediaType selectedContentType, 
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
		
		try {
			String token = body.getToken();
			String jti = this.pasTokenService.getJTIFromToken(token);
			
			logger.info("jti: " + jti);
			logger.info("getURI: " + request.getURI().getPath());
			
			this.redisService.storeRequest(jti, request.getURI().getPath());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return body;
	}
}
