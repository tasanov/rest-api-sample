package com.pps.advice.response;

import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.pps.sec.AuthenticatedUser;
import com.pps.token.PASTokenService;

@ControllerAdvice
@Order(1)
public class AddTokenDisclaimerClaim implements ResponseBodyAdvice<AddTokenDisclaimerClaimIdentifier> {

	private static final Logger logger = LoggerFactory.getLogger(AddTokenDisclaimerClaim.class);
	
	@Autowired
	private PASTokenService pasTokenService;
	
	private static final Class<?> myClazz = AddTokenDisclaimerClaimIdentifier.class;
	
	@Override
	public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> converterType) {
		Class<?> clazz = methodParameter.getMethod().getReturnType();
		
		return myClazz.isAssignableFrom(clazz);
	}

	@Override
	public AddTokenDisclaimerClaimIdentifier beforeBodyWrite(AddTokenDisclaimerClaimIdentifier body, MethodParameter returnType, MediaType selectedContentType, 
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		
		if (securityContext != null && securityContext.getAuthentication() != null) {
			AuthenticatedUser authenticatedUser = (AuthenticatedUser) securityContext.getAuthentication().getPrincipal();
				
			if (authenticatedUser != null) {
				try {
					String newToken = this.pasTokenService.generateAuthTokenWithDisclaimer(authenticatedUser);
					
					body.setToken(newToken);
					
					// TODO invalidate old token; spring context
				} catch (JoseException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		
		return body;
	}
}
