package com.pps.advice.response;

public interface AddTokenDisclaimerClaimIdentifier {

	public void setToken(String token);
}
