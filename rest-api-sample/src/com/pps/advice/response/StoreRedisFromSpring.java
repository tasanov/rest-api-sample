package com.pps.advice.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.pps.redis.RedisService;
import com.pps.sec.AuthenticatedUser;
import com.pps.token.PASTokenService;

@ControllerAdvice
@Order(99)
public class StoreRedisFromSpring implements ResponseBodyAdvice<StoreRedisFromSpringIdentifier> {

	private static final Logger logger = LoggerFactory.getLogger(StoreRedisFromSpring.class);
	
	@Autowired
	private PASTokenService pasTokenService;
	
	@Autowired
	private RedisService redisService;
	
	private static final Class<?> myClazz = StoreRedisFromSpringIdentifier.class;
	
	@Override
	public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> converterType) {
		Class<?> clazz = methodParameter.getMethod().getReturnType();
		
		return myClazz.isAssignableFrom(clazz);
	}

	@Override
	public StoreRedisFromSpringIdentifier beforeBodyWrite(StoreRedisFromSpringIdentifier body, MethodParameter returnType, MediaType selectedContentType, 
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
		
		try {
			SecurityContext securityContext = SecurityContextHolder.getContext();
			
			if (securityContext != null && securityContext.getAuthentication() != null) {
				AuthenticatedUser authenticatedUser = (AuthenticatedUser) securityContext.getAuthentication().getPrincipal();
					
				if (authenticatedUser != null) {
					String jti = this.pasTokenService.getJTIFromAuthenticatedUser(authenticatedUser);
					
					logger.info("jti: " + jti);
					logger.info("getURI: " + request.getURI().getPath());
					
					this.redisService.storeRequest(jti, request.getURI().getPath());
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return body;
	}
}
