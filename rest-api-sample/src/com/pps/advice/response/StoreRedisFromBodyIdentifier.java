package com.pps.advice.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface StoreRedisFromBodyIdentifier {

	@JsonIgnore
	public String getToken();
}
