package com.pps.advice.response;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pps.object.ResponseStatus;
import com.pps.object.response.PASResponse;

@ControllerAdvice
public class PASExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(PASExceptionHandler.class);
	
	@Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
		logException(e, request);
		
		PASResponse response = new PASResponse();
		response.setStatus(ResponseStatus.FAILURE);
		response.setErrorMessage("Bad request");
		
		return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler({AccessDeniedException.class})
	public ResponseEntity<Object> handleAccessDeniedException(Exception e, HttpServletRequest request) {
		logException(e, request);
		
		PASResponse response = new PASResponse();
		response.setStatus(ResponseStatus.FAILURE);
		response.setErrorMessage("Access denied");
		
		return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.FORBIDDEN);
	}
	
	public static void handleUnauthorizedException(Exception e, HttpServletRequest request, HttpServletResponse response) {
		logException(e, request);
		
		PASResponse pasResponse = new PASResponse();
		pasResponse.setStatus(ResponseStatus.FAILURE);
		pasResponse.setErrorMessage("Unauthorized");
		
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setCharacterEncoding(StandardCharsets.UTF_8.name());
		
		String jsonOutput = null;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			jsonOutput = mapper.writeValueAsString(pasResponse);
		} catch (JsonProcessingException ex) {
			logger.error(ex.getMessage(), ex);
		}
		
		try {
			response.getWriter().print(jsonOutput);
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}
	}
	
	private static void logException(Exception e, WebRequest request) {
		logger.error(e.getMessage(), e);
		
		// TODO what to log?
	}
	
	private static void logException(Exception e, HttpServletRequest request) {
		logger.error(e.getMessage(), e);
		
		// TODO what to log?
	}
}
