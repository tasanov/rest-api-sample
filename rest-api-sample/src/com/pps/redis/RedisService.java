package com.pps.redis;

import java.time.LocalDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pps.object.redis.APIRequest;
import com.pps.object.redis.UserHistory;
import com.pps.object.redis.UserHistoryRepository;

@Component
public class RedisService {
	
	private static final Logger logger = LoggerFactory.getLogger(RedisService.class);
	
	@Autowired
	private UserHistoryRepository userHistoryRepository;
	
	public void storeRequest(String jti, String url) {
		logger.info("storing [" + jti + "]");
		
		try {
			UserHistory userHistory = this.findUserByUUID(jti);
			
			if (userHistory == null) {
				userHistory = new UserHistory(jti);
				
				logger.info("does not exist");
			}
			
			APIRequest apiRequest = new APIRequest(UUID.randomUUID().toString(), url, LocalDateTime.now());
			
			userHistory.addAPIRequest(apiRequest);
			
			userHistoryRepository.save(userHistory);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public UserHistory findUserWorkflowHistory(String jti) {
		return this.findUserByUUID(jti);
	}
	
	private UserHistory findUserByUUID(String uuid) {
		UserHistory userHistory = userHistoryRepository.findOne(uuid);
		
		return userHistory;
	}
}
