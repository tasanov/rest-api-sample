package com.pps.token;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwa.AlgorithmConstraints.ConstraintType;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwk.EllipticCurveJsonWebKey;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.pps.object.ClientApps;
import com.pps.object.TokenClaims;
import com.pps.object.TokenType;
import com.pps.object.request.AuthenticateRequest;
import com.pps.sec.AuthenticatedUser;

@Component
public class PASTokenService implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(PASTokenService.class);
	
	private AlgorithmConstraints jwsAlgConstraints;
	private AlgorithmConstraints jweAlgConstraints;
	private AlgorithmConstraints jweEncConstraints;
	
	private EllipticCurveJsonWebKey senderJwk;
	private EllipticCurveJsonWebKey receiverJwk;
	
	@Value("${key.sender.private}")
	private String senderPrivateKey;
	
	@Value("${key.receiver.private}")
	private String receiverPrivateKey;
	
	public PASTokenService() {
		this.jwsAlgConstraints = new AlgorithmConstraints(ConstraintType.WHITELIST, AlgorithmIdentifiers.ECDSA_USING_P521_CURVE_AND_SHA512);
		this.jweAlgConstraints = new AlgorithmConstraints(ConstraintType.WHITELIST, KeyManagementAlgorithmIdentifiers.ECDH_ES_A256KW);
		this.jweEncConstraints = new AlgorithmConstraints(ConstraintType.WHITELIST, ContentEncryptionAlgorithmIdentifiers.AES_256_CBC_HMAC_SHA_512);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			this.senderJwk = (EllipticCurveJsonWebKey) JsonWebKey.Factory.newJwk(this.senderPrivateKey);
			this.receiverJwk = (EllipticCurveJsonWebKey) JsonWebKey.Factory.newJwk(this.receiverPrivateKey);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			throw e;
		}
	}
	
	public String generateAuthToken(AuthenticateRequest authRequest) throws JoseException {
		JwtClaims claims = createClaims(authRequest.getAppId(), authRequest.getUserId(), TokenType.AUTH_TOKEN);

		return generateToken(claims);
	}
	
	public String generateAuthTokenWithDisclaimer(AuthenticatedUser authenticatedUser) throws JoseException {
		JwtClaims claims = createClaims(ClientApps.valueOf(authenticatedUser.getAudiences().get(0)), authenticatedUser.getUsername(), TokenType.AUTH_TOKEN);
		claims.setClaim(TokenClaims.ACCEPTED_DISCLAIMER.name(), true);

		return generateToken(claims);
	}
	
	public String generateTempToken(ClientApps appId) throws JoseException {
		JwtClaims claims = createClaims(appId, UUID.randomUUID().toString(), TokenType.TEMP_TOKEN);

		return generateToken(claims);
	}
	
	public String regenerateToken(AuthenticatedUser authenticatedUser) throws JoseException {
		JwtClaims claims = createClaims(
					ClientApps.valueOf(authenticatedUser.getAudiences().get(0)), 
					authenticatedUser.getUsername(), 
					TokenType.valueOf((String) authenticatedUser.getClaims().get(TokenClaims.TOKEN_TYPE.name()))
				);

		return generateToken(claims);
	}
	
	private String generateToken(JwtClaims claims) throws JoseException {
		String innerJwt = createJWS(claims);
		
		JsonWebEncryption jwe = createJWE();
		jwe.setPayload(innerJwt);

		// Produce the JWE compact serialization, which is the complete JWT/JWE
		// representation,
		// which is a string consisting of five dot ('.') separated
		// base64url-encoded parts in the form
		// Header.EncryptedKey.IV.Ciphertext.AuthenticationTag
		String jwt = jwe.getCompactSerialization();

		return jwt;
	}
	
	private JwtClaims createClaims(ClientApps appId, String userId, TokenType tokenType) {
		JwtClaims claims = new JwtClaims();
		claims.setIssuer("PAS"); // who creates the token and signs it
		claims.setAudience(appId.name()); // to whom the token is intended to be sent
		claims.setExpirationTimeMinutesInTheFuture(2); // time when the token will expire (10 minutes from now)
		claims.setGeneratedJwtId(); // a unique identifier for the token
		claims.setIssuedAtToNow(); // when the token was issued/created (now)
		claims.setNotBeforeMinutesInThePast(1); // time before which the token is not yet valid (2 minutes ago)
		claims.setSubject(userId); // the subject/principal is whom the token is about
		claims.setClaim(TokenClaims.TOKEN_TYPE.name(), tokenType); // additional claims/attributes about the subject can be added
		claims.setClaim(TokenClaims.TOKEN_JTI.name(), UUID.randomUUID().toString());
		
		// add original time created
		// don't add token on every refresh
		// token/refresh call generate new token
		
		return claims;
	}
	
	private String createJWS(JwtClaims claims) throws JoseException {
		// A JWT is a JWS and/or a JWE with JSON claims as the payload.
		// In this example it is a JWS nested inside a JWE
		// So we first create a JsonWebSignature object.
		JsonWebSignature jws = new JsonWebSignature();

		// The payload of the JWS is JSON content of the JWT Claims
		jws.setPayload(claims.toJson());

		// The JWT is signed using the sender's private key
		jws.setKey(this.senderJwk.getPrivateKey());

		// Set the Key ID (kid) header because it's just the polite thing to do.
		// We only have one signing key in this example but a using a Key ID helps
		// facilitate a smooth key rollover process
		jws.setKeyIdHeaderValue(this.senderJwk.getKeyId());

		// Set the signature algorithm on the JWT/JWS that will integrity protect the
		// claims
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.ECDSA_USING_P521_CURVE_AND_SHA512);

		// Sign the JWS and produce the compact serialization, which will be the inner
		// JWT/JWS
		// representation, which is a string consisting of three dot ('.') separated
		// base64url-encoded parts in the form Header.Payload.Signature
		return jws.getCompactSerialization();
	}
	
	private JsonWebEncryption createJWE() {
		// The outer JWT is a JWE
		JsonWebEncryption jwe = new JsonWebEncryption();

		// The output of the ECDH-ES key agreement will encrypt a randomly generated
		// content encryption key
		jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.ECDH_ES_A256KW);

		// The content encryption key is used to encrypt the payload
		// with a composite AES-CBC / HMAC SHA2 encryption algorithm
		String encAlg = ContentEncryptionAlgorithmIdentifiers.AES_256_CBC_HMAC_SHA_512;
		jwe.setEncryptionMethodHeaderParameter(encAlg);

		// We encrypt to the receiver using their public key
		jwe.setKey(this.receiverJwk.getPublicKey());
		jwe.setKeyIdHeaderValue(this.receiverJwk.getKeyId());

		// A nested JWT requires that the cty (Content Type) header be set to "JWT" in
		// the outer JWT
		jwe.setContentTypeHeaderValue("JWT");
		
		return jwe;
	}
	
	public AuthenticatedUser parseToken(String token) throws JoseException {
		try {
			JwtConsumer jwtConsumer = createJWTConsumer();
			String decryptedToken = decryptToken(token);
			
			// Validate the JWT and process it to the Claims
			JwtClaims jwtClaims = jwtConsumer.processToClaims(decryptedToken);
			
			return new AuthenticatedUser(jwtClaims.getSubject(), jwtClaims.getAudience(), jwtClaims.getClaimsMap());
		} catch (InvalidJwtException e) {
			logger.error(e.getMessage(), e);
		} catch (MalformedClaimException e) {
			logger.error(e.getMessage(), e);
		}

		return null;
	}
	
	private String decryptToken(String token) throws JoseException {
		// That other party, the receiver, can then use JsonWebEncryption to decrypt the
		// message.
		JsonWebEncryption receiverJwe = new JsonWebEncryption();

		// Set the algorithm constraints based on what is agreed upon or expected from
		// the sender
		receiverJwe.setAlgorithmConstraints(this.jweAlgConstraints);
		receiverJwe.setContentEncryptionAlgorithmConstraints(this.jweEncConstraints);

		// Set the compact serialization on new Json Web Encryption object
		receiverJwe.setCompactSerialization(token);

		// Symmetric encryption, like we are doing here, requires that both parties have
		// the same key.
		// The key will have had to have been securely exchanged out-of-band somehow.
		receiverJwe.setKey(this.receiverJwk.getPrivateKey());

		// Get the message that was encrypted in the JWE. This step performs the actual
		// decryption steps.
		return receiverJwe.getPlaintextString();
	}
	
	private JwtConsumer createJWTConsumer() {
		JwtConsumer jwtConsumer = new JwtConsumerBuilder().setRequireExpirationTime() // the JWT must have an expiration time
				.setMaxFutureValidityInMinutes(35) // but the expiration time can't be too crazy
				.setRequireSubject() // the JWT must have a subject claim
				.setExpectedIssuer("PAS") // whom the JWT needs to have been issued by
				.setExpectedAudience(Arrays.stream(ClientApps.values()).map(Enum::name).toArray(String[]::new))
				.setVerificationKey(this.senderJwk.getPublicKey()) // verify the signature with the sender's public key
				.setJwsAlgorithmConstraints(this.jwsAlgConstraints) // limits the acceptable signature algorithm(s)
				.build();
		
		return jwtConsumer;
	}
	
	public String getJTIFromToken(String token) throws JoseException {
		AuthenticatedUser authenticatedUser = this.parseToken(token);
		
		return this.getJTIFromAuthenticatedUser(authenticatedUser);
	}
	
	public String getJTIFromAuthenticatedUser(AuthenticatedUser authenticatedUser) throws JoseException {
		String result = null;
		
		Map<String, Object> claims = authenticatedUser.getClaims();
		
		if (claims != null && claims.size() > 0) {
			result = (String) claims.get(TokenClaims.TOKEN_JTI.name());
		}
		
		return result;
	}
}
