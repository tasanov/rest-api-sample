package com.pps.app_config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.pps.sec.SpringAuthenticationFailureHandler;
import com.pps.sec.SpringAuthenticationProvider;
import com.pps.sec.SpringAuthenticationSuccessHandler;
import com.pps.sec.SpringAuthenticationTokenFilter;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurity extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private SpringAuthenticationFailureHandler springAuthenticationFailureHandler;
	
	@Autowired
	private SpringAuthenticationSuccessHandler springAuthenticationSuccessHandler;
	
	@Autowired
	private SpringAuthenticationProvider springAuthenticationProvider;
	
	@Bean
	public SpringAuthenticationTokenFilter getAuthenticationFilter() throws Exception {
		SpringAuthenticationTokenFilter filter = new SpringAuthenticationTokenFilter();
		
		filter.setAuthenticationManager(authenticationManagerBean());
		filter.setAuthenticationSuccessHandler(springAuthenticationSuccessHandler);
		filter.setAuthenticationFailureHandler(springAuthenticationFailureHandler);
		
		return filter;
	}
	
	@Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        AuthenticationManager authenticationManager = new ProviderManager(Arrays.asList((AuthenticationProvider) springAuthenticationProvider));
        
        return authenticationManager;
    }
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/authenticate");
		web.ignoring().antMatchers("/requestToken");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.addFilterBefore(getAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        http.authorizeRequests().anyRequest().authenticated();
	}
}
