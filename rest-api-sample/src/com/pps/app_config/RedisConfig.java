package com.pps.app_config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@Configuration
@EnableRedisRepositories(basePackages = "com.pps.object.redis")
@PropertySources({
	@PropertySource("classpath:redis.properties")
})
public class RedisConfig {

	@Value("${redis.cluster.nodes}")
	private String[] redisClusterNodes;
	
	@Bean
	public RedisConnectionFactory redisConnectionFactory() {
		return new JedisConnectionFactory(new RedisClusterConfiguration(Arrays.asList(this.redisClusterNodes)));
	}
	
	@Bean
	public RedisTemplate<?, ?> redisTemplate() {
		RedisTemplate<byte[], byte[]> template = new RedisTemplate<byte[], byte[]>();
		template.setConnectionFactory(redisConnectionFactory());
		
		return template;
	}
}
