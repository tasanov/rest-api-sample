package com.pps.sec;

import org.springframework.security.core.AuthenticationException;

public class CustomException extends AuthenticationException {

	private static final long serialVersionUID = 806638843813915089L;

	public CustomException(String msg) {
		super(msg);
	}
}
