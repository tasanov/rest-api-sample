package com.pps.sec;

import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.pps.token.PASTokenService;

@Component
public class SpringAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(SpringAuthenticationProvider.class);

	@Autowired
	private PASTokenService pasTokenService;
	
    @Override
    public boolean supports(Class<?> authentication) {
        return (SpringAuthenticationToken.class.isAssignableFrom(authentication));
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    	
    }

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		SpringAuthenticationToken jwtAuthenticationToken = (SpringAuthenticationToken) authentication;
		String token = jwtAuthenticationToken.getToken();

		try {
			AuthenticatedUser authenticatedUser = this.pasTokenService.parseToken(token);
			
			if (authenticatedUser == null) {
				logger.error("invalid token");
				
				throw new CustomException("invalid token");
			}
			
			return authenticatedUser;
		} catch (Exception e) {
			logger.error(e.getMessage() , e);
			
			throw new CustomException("invalid token");
		}
	}
}
