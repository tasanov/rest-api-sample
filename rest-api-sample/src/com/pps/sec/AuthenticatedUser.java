package com.pps.sec;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class AuthenticatedUser implements UserDetails {

	private static final long serialVersionUID = 8576205750916593311L;
	
    private final String userId;
    private final List<String> audiences;
    private final Map<String, Object> claims;

    public AuthenticatedUser(String userId, List<String> audiences, Map<String, Object> claims) {
        this.userId = userId;
        this.audiences = audiences;
        this.claims = claims;
    }

    @JsonIgnore
    public List<String> getAudiences() {
        return audiences;
    }

    @JsonIgnore
    public Map<String, Object> getClaims() {
        return claims;
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return userId;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return null;
    }

	@Override
    @JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	// TODO do not print token
	@Override
	public String toString() {
		return "AuthenticatedUser [username=" + userId + "]";
	}
}
