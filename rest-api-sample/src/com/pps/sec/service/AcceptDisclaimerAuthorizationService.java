package com.pps.sec.service;

import org.springframework.stereotype.Component;

import com.pps.object.request.AcceptDisclaimerRequest;
import com.pps.sec.AuthenticatedUser;

@Component
public class AcceptDisclaimerAuthorizationService extends AuthorizationService<AcceptDisclaimerRequest> {

	@Override
	public boolean checkAuthorization(AuthenticatedUser authenticatedUser, AcceptDisclaimerRequest in) {
		return this.validateCommonClaims(authenticatedUser, in) 
				&& this.validateAuthenticatedToken(authenticatedUser, false) 
				&& this.validateWorkflow(authenticatedUser);
	}
}
