package com.pps.sec.service;

import org.springframework.stereotype.Component;

import com.pps.object.request.CheckUsernameRequest;
import com.pps.sec.AuthenticatedUser;

@Component
public class CheckUsernameAuthorizationService extends AuthorizationService<CheckUsernameRequest> {

	@Override
	public boolean checkAuthorization(AuthenticatedUser authenticatedUser, CheckUsernameRequest in) {
		return this.validateCommonClaims(authenticatedUser, in) && this.validateTemporaryToken(authenticatedUser);
	}
}
