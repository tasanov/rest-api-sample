package com.pps.sec.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pps.object.ClientApps;
import com.pps.object.TokenClaims;
import com.pps.object.TokenType;
import com.pps.object.redis.UserHistory;
import com.pps.object.request.PASRequest;
import com.pps.redis.RedisService;
import com.pps.sec.AuthenticatedUser;

@Component
public abstract class AuthorizationService<T> {

	private static final Logger logger = LoggerFactory.getLogger(AuthorizationService.class);
	
	@Autowired
	private RedisService redisService;
	
	public abstract boolean checkAuthorization(AuthenticatedUser authenticatedUser, T in);
	
	protected boolean validateCommonClaims(AuthenticatedUser authenticatedUser, T in) {
		return this.validateAudience(authenticatedUser, in) && this.validateJTI(authenticatedUser);
	}
	
	protected boolean validateAudience(AuthenticatedUser authenticatedUser, T in) {
		logger.info("checking audience");
		
		try {
			PASRequest request = (PASRequest) in;
			
			List<String> audiences = authenticatedUser.getAudiences();
			
			if (audiences == null || audiences.size() != 1) {
				logger.error("incorrect number of audiences in principal");
				
				return false;
			}
			
			ClientApps fromPrincipal = ClientApps.valueOf(audiences.get(0));
			ClientApps fromRequest = request.getAppId();
			
			return fromPrincipal == fromRequest;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			return false;
		}
	}
	
	protected boolean validateJTI(AuthenticatedUser authenticatedUser) {
		logger.info("checking JTI");
		
		try {
			Map<String, Object> claims = authenticatedUser.getClaims();
			
			if (claims != null && claims.size() > 0) {
				String tokenJTI = (String) claims.get(TokenClaims.TOKEN_JTI.name());
				
				if (tokenJTI != null) {
					UUID.fromString(tokenJTI);
					
					return true;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return false;
	}
	
	protected boolean validateTemporaryToken(AuthenticatedUser authenticatedUser) {
		logger.info("checking temp token");
		
		try {
			Map<String, Object> claims = authenticatedUser.getClaims();
			
			if (claims != null && claims.size() > 0) {
				TokenType tokenType = TokenType.valueOf((String) claims.get(TokenClaims.TOKEN_TYPE.name()));
				
				if (tokenType == TokenType.TEMP_TOKEN) {
					String subject = authenticatedUser.getUsername();
					
					UUID.fromString(subject);
					
					return true;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return false;
	}
	
	protected boolean validateAuthenticatedToken(AuthenticatedUser authenticatedUser, boolean disclaimerCheck) {
		logger.info("checking auth token");
		
		try {
			Map<String, Object> claims = authenticatedUser.getClaims();
			
			if (claims != null && claims.size() > 0) {
				TokenType tokenType = TokenType.valueOf((String) claims.get(TokenClaims.TOKEN_TYPE.name()));
				
				if (tokenType == TokenType.AUTH_TOKEN) {
					if (disclaimerCheck == true) {
						boolean didAcceptDisclaimer = (boolean) claims.get(TokenClaims.ACCEPTED_DISCLAIMER.name());
						
						return didAcceptDisclaimer;
					}
					
					return true;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return false;
	}

	protected boolean validateWorkflow(AuthenticatedUser authenticatedUser) {
		logger.info("checking workflow");
		
		try {
			Map<String, Object> claims = authenticatedUser.getClaims();
			
			if (claims != null && claims.size() > 0) {
				String jti = (String) claims.get(TokenClaims.TOKEN_JTI.name());
				
				UserHistory userHistory = this.redisService.findUserWorkflowHistory(jti);
				
				logger.info(userHistory.toString());

				// TODO validate here
				
				return true;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return false;
	}
}
