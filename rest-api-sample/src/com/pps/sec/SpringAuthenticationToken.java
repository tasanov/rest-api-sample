package com.pps.sec;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class SpringAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 5136768762066770366L;
	
	private String token;

    public SpringAuthenticationToken(String token) {
        super(null, null);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}