package com.pps.object.redis;

import java.io.Serializable;
import java.time.LocalDateTime;

public class APIRequest implements Serializable {

	private static final long serialVersionUID = -5988714655131655644L;
	
	private String uuid;
	private String url;
	private LocalDateTime requestTime;
	
	public APIRequest(String uuid, String url, LocalDateTime requestTime) {
		this.uuid = uuid;
		this.url = url;
		this.requestTime = requestTime;
	}

	public String getUrl() {
		return url;
	}

	public LocalDateTime getRequestTime() {
		return requestTime;
	}

	public String getUuid() {
		return uuid;
	}

	@Override
	public String toString() {
		return "APIRequest [uuid=" + uuid + ", url=" + url + ", requestTime=" + requestTime + "]";
	}
}
