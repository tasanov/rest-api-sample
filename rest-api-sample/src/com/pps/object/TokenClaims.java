package com.pps.object;

public enum TokenClaims {

	ACCEPTED_DISCLAIMER,
	TOKEN_TYPE,
	TOKEN_JTI;
}
