package com.pps.object.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import com.pps.object.ResponseStatus;

public class PASResponse {

	private ResponseStatus status;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String errorMessage;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String idToken;
	
	public PASResponse() {}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}

	// TODO do not print token
	@Override
	public String toString() {
		return "PASResponse [status=" + status + ", errorMessage=" + errorMessage + ", idToken=" + idToken + "]";
	}
}
