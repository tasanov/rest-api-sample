package com.pps.object.response;

public class CheckUsernameResponse extends PASResponse {

	private boolean isAvailable;
	
	public CheckUsernameResponse() {
		super();
	}
	
	public CheckUsernameResponse(boolean isAvailable) {
		super();
		
		this.isAvailable = isAvailable;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	// TODO do not print token
	@Override
	public String toString() {
		return "CheckUsernameResponse [status=" + super.getStatus() + ", errorMessage=" + super.getErrorMessage() 
										+ ", idToken=" + super.getIdToken() + ", isAvailable=" + this.isAvailable + "]";
	}
}
