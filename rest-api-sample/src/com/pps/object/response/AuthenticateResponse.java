package com.pps.object.response;

import com.pps.advice.response.StoreRedisFromBodyIdentifier;

public class AuthenticateResponse extends PASResponse implements StoreRedisFromBodyIdentifier {
	
	public AuthenticateResponse() {
		super();
	}
	
	@Override
	public String getToken() {
		return super.getIdToken();
	}

	// TODO do not print token
	@Override
	public String toString() {
		return "AuthenticateResponse [status=" + super.getStatus() + ", errorMessage=" + super.getErrorMessage() + ", idToken=" + super.getIdToken() + "]";
	}
}
