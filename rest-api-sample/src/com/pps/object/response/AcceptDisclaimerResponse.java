package com.pps.object.response;

import com.pps.advice.response.AddTokenDisclaimerClaimIdentifier;
import com.pps.advice.response.StoreRedisFromSpringIdentifier;

public class AcceptDisclaimerResponse extends PASResponse implements AddTokenDisclaimerClaimIdentifier, StoreRedisFromSpringIdentifier {

	public AcceptDisclaimerResponse() {
		super();
	}

	@Override
	public void setToken(String token) {
		super.setIdToken(token);
	}
	
	@Override
	public String toString() {
		return "AcceptDisclaimerResponse [status=" + super.getStatus() + ", errorMessage=" + super.getErrorMessage() + "]";
	}
}
