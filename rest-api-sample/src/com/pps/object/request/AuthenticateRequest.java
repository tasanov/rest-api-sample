package com.pps.object.request;

import java.util.Arrays;

import com.pps.object.ClientApps;

public class AuthenticateRequest extends PASRequest {

	private String userId;
	private char[] password;
	
	public AuthenticateRequest() {
		super();
	}
	
	public AuthenticateRequest(ClientApps appId, String userId, char[] password) {
		super(appId);
		
		this.userId = userId;
		this.password = password;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	// TODO do not print password
	@Override
	public String toString() {
		return "AuthenticateRequest [appId=" + super.getAppId() + ", userId=" + userId + ", password=" + Arrays.toString(password) + "]";
	}
}
