package com.pps.object.request;

import com.pps.object.ClientApps;

public class PASRequest {

	private ClientApps appId;
	
	public PASRequest() {}

	public PASRequest(ClientApps appId) {
		this.appId = appId;
	}

	public ClientApps getAppId() {
		return appId;
	}

	public void setAppId(ClientApps appId) {
		this.appId = appId;
	}

	@Override
	public String toString() {
		return "PASRequest [appId=" + appId + "]";
	}
}
