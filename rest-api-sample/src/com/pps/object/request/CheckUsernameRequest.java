package com.pps.object.request;

import com.pps.object.ClientApps;

public class CheckUsernameRequest extends PASRequest {

	private String username;
	
	public CheckUsernameRequest() {
		super();
	}
	
	public CheckUsernameRequest(ClientApps appId, String username) {
		super(appId);
		
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "CheckUsernameRequest [appId=" + super.getAppId() + "username=" + username + "]";
	}
}
