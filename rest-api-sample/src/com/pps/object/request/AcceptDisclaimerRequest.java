package com.pps.object.request;

import com.pps.object.ClientApps;

public class AcceptDisclaimerRequest extends PASRequest {

	public AcceptDisclaimerRequest() {
		super();
	}
	
	public AcceptDisclaimerRequest(ClientApps appId) {
		super(appId);
	}
	
	@Override
	public String toString() {
		return "AcceptDisclaimerRequest [appId=" + super.getAppId() + "]";
	}
}
